What is this ?
==============

This repository contains the needed files and steps to setup OpenGL.



Needed files?
=============

Simply download [OpenGL_with_glew_vc12_x86.zip](https://bitbucket.org/herohuyongtao/opengl-setup/downloads/OpenGL_with_glew_vc12_x86.zip) from `Downloads` page and put anywhere to your PC, assume `C:\Libs\OpenGL`. 

Noted that, the embedded `glew` is builded under `vc12-x86` mode, you will need to follow the following two steps if you want to use it for other platforms:

1. Download [OpenGL basic files](https://bitbucket.org/herohuyongtao/opengl-setup/downloads/OpenGL_basic.zip) from `Downloads` page and put anywhere to your PC, assume `C:\Libs\OpenGL`.

2. GLEW package. 

	* Download its source from [GLEW homepage](http://glew.sourceforge.net/).
	
	* Build GLEW in the target compiler (platform toolset). For example, if you want to use it under `v100` platform toolset, build it under `Visual Studio 2010`. Only 2 of 4 projects (i.e. `glew_shared` and `glew_static`) are needed to build.
 
	* Then you can copy built files also to OpenGL basic folders for easy setup.
	
		- Copy `glew.h`, `glxew.h` and `wglew.h` to `C:\Libs\OpenGL\include\GL`.
		
		- Copy `glew32.lib`, `glew32d.lib`, `glew32s.lib` and `glew32sd.lib` to `C:\Libs\OpenGL\lib`.
		
		- Copy `glew32.dll` and `glew32d.dll` to `C:\Libs\OpenGL\bin`.
		
		

Setup in VC++
=============

1. Add `C:\Libs\OpenGL\include` to `Additional Include Directories`.

2. Add `C:\Libs\OpenGL\lib` to `Additional Library Directories`.

3. Add `opengl32.lib;glu32.lib;glut32.lib;glew32.lib;GlAux.Lib;` to `Additional Dependencies`.

4. Add `xcopy "C:\Libs\OpenGL\bin\*.dll" "$(OutDir)" /Y /Q /D` to `Properties > Build Events > Command Line`.

**Or if you're using CMake, you can:**

1. Add system variable called `OpenGL_DIR` with value `C:\Libs\OpenGL`.

2. Add `C:\Libs\OpenGL\bin` to `Path`.

3. Use the following commands in `CMakeLists.txt`:

		# OpenGL lib
		include_directories($ENV{OpenGL_DIR}/include)

		# link libs
		target_link_libraries(yourProj 
			$ENV{OpenGL_DIR}/lib/Glaux.lib
			debug $ENV{OpenGL_DIR}/lib/glew32d.lib	
			debug $ENV{OpenGL_DIR}/lib/glew32sd.lib
			optimized $ENV{OpenGL_DIR}/lib/glew32.lib
			optimized $ENV{OpenGL_DIR}/lib/glew32s.lib
			$ENV{OpenGL_DIR}/lib/GLU32.lib
			$ENV{OpenGL_DIR}/lib/glui32.lib
			$ENV{OpenGL_DIR}/lib/glut32.lib
			$ENV{OpenGL_DIR}/lib/OPENGL32.lib
		)